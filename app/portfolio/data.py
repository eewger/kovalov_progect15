certificats = [
    {
        "name" : "CPA: Programming Essentials in C++",
        "platform": "Cisco",
        "instractorName": "Mykola Kozlenko",
        "date": "3 Jun 2022",
        "photoName": "c++.png"
    },
    {
        "name" : "Python and Flask Bootcamp",
        "platform": "Udemy",
        "instractorName": "Jose Portilla",
        "date": "29 Aug 2023",
        "photoName": "pythonFlask.jpg"
    }
]